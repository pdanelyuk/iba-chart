import React from 'react';
import '../../ChartTable.css';

let ChartTableHeader = (props) => {
    const { tableHeaderItems, handleSorting, sortItemKey } = props

    // Array.prototype.move = function (from, to) {
    //     this.splice(to, 0, this.splice(from, 1)[0]);
    // };

    // tableHeaderItems.move(tableHeaderItems.length - 1, 0);
    // tableHeaderItems.move(tableHeaderItems.length - 1, 0);
    // console.log(tableHeaderItems)

    //tableHeaderItems.unshift(tableHeaderItems.pop())
    //tableHeaderItems.unshift(tableHeaderItems.pop())

    return (
        <thead>
            <tr>
                {tableHeaderItems.map((item, index) => {
                    var tableHeaderItemNameDisplay = "";
                    let testClassName = item === sortItemKey ? `selected` : null

                    if ((index === 0) || (index === 1)) {
                        // var tableItemClassName = `${style[`tableItem-name-${item}`]}`;
                    } else {
                        tableHeaderItemNameDisplay = item.replace('y', '');
                    }

                    return (
                        <th
                            key={`${item}${index}`}
                            id={`${item}`}
                            className={`tableHeaderItem tableItemClassName ${testClassName}`}
                            onClick={() => handleSorting(item)}
                        >
                            <div>{tableHeaderItemNameDisplay}</div>
                        </th>
                    )
                })}
            </tr>
        </thead>
    )
}

export default ChartTableHeader;
