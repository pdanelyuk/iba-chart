import React from 'react'
import style from '../ChartTable/ChartTable.module.css';

const ChartTableBody = (props) => {
    const { cocktails, tableHeaderItems } = props

    return (
        <tbody>
            {cocktails.map((item, index) => {
                return (
                    <tr key={`${item}${index}`}>
                        {tableHeaderItems.map((itemX, indexX) => {
                            let tableItemClassName = `${style[`tableItem-empty`]}`;
                            let tableItemName = item[itemX];

                            if ((indexX === 0) || (indexX === 1)) {
                                tableItemClassName = `${style[`tableItem-name`]} ${style[`tableItem-name-${itemX}`]}`;
                            } else if (item[itemX] !== "") {
                                tableItemName = '';
                                tableItemClassName = `${style[`tableItem-${item[itemX]}`]}`;
                            } else {
                                tableItemName = '';
                            }

                            if (indexX === 1) {
                                //tableItemName = tableItemName.replace('[','').replace(']','').replace(', ',' (');
                                //if ( (tableItemName.indexOf("(") > -1) && (tableItemName.indexOf(")") === -1) )  { tableItemName += ')';  } 
                            }

                            return <td key={`${itemX}${indexX}`} className={`${style.tableItem} ${tableItemClassName}`}><div>{tableItemName}</div></td>
                        })}
                    </tr>
                )
            })}
        </tbody>
    )
}

export default ChartTableBody
