import React, { useState } from 'react';
import ChartTableBody from '../ChartTableBody/ChartTableBody';
import ChartTableHeader from '../ChartTableHeader/ChartTableHeader';
import { cocktailsData } from '../../utils/data';
import style from './ChartTable.module.css';

const ChartTable = () => {
    const [sortItemKey, setSortItemKey] = useState(null)
    const [cocktails, setCocktails] = useState(cocktailsData)
    const firstElement = cocktails[0]
    const tableHeaderItems = Object.keys(firstElement)

    const handleSorting = (sortKey) => {
        setSortItemKey(sortKey);

        const sortOrder = ['PD', 'AD', 'LD', 'PC', 'SC', 'UN', 'CC', 'NE', ''];
        let ordering = {}

        for (let i = 0; i < sortOrder.length; i++) {
            ordering[sortOrder[i].toLowerCase()] = i;
        }

        let sortedArray = cocktails.sort(function (a, b) {
            let itemA = ordering[a[sortKey].toLowerCase()]
            let itemB = ordering[b[sortKey].toLowerCase()]

            if (itemA < itemB) {
                return -1
            } else if (itemA > itemB) {
                return 1
            } else {
                let itemAA = a['Eng'].toLowerCase()
                let itemBB = b['Eng'].toLowerCase()
                if (itemAA < itemBB) {
                    return -1
                } else if (itemAA > itemBB) {
                    return 1
                } else {
                    return 0
                }
            }
        })

        setCocktails([...sortedArray])
    }

    return (
        <table className={style.table}>
            <ChartTableHeader tableHeaderItems={tableHeaderItems} handleSorting={handleSorting} sortItemKey={sortItemKey} />
            <ChartTableBody cocktails={cocktails} tableHeaderItems={tableHeaderItems} />
        </table>
    );
}

export default ChartTable;
