import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import ChartTable from './components/ChartTable/ChartTable';
import './App.css';

const App = () => {
    return (
        <Router>
            <div className="App">
                <header className="App-header">
                    <h1>IBA cocktails history</h1>
                </header>
                <article>
                    <div>
                        <Route path='/charttable'>
                            <ChartTable />
                        </Route>
                    </div>
                </article>
                <footer>

                </footer>
            </div>
        </Router>
    );
}

export default App;
