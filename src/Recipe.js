import React from 'react';
import style from './Recipe.module.css'
import state from './state';

let Recipe = (props) => {

  const currentPageId = props.match.params.id;

  const currentPageItem = state.cocktails.filter( item => {
    return currentPageId === item.id
  } );

  const {id, name, image, ingredientName01, ingredientAmount01, ingredientAmountUnit01} = currentPageItem[0];

  return (
    <div className="RecipeWrapper">
      <div className="CocktailName">
        {name}
      </div>
      <div className="CocktailPicture">
        <img src={image} className={style.CocktailImage} alt="Cocktail name" />
      </div>
      <div className="Ingredients">
        <div className={style.Ingredient}>
          <div className="IngredientName">{ingredientName01}</div>
          <div className="IngredientAmount">{ingredientAmount01}</div>
          <div className="IngredientAmountUnit">{ingredientAmountUnit01}</div>
        </div>
      </div>
    </div>
  );
}

export default Recipe;
